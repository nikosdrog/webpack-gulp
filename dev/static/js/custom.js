$(function() {
  fancy();
});

$(document).ready(function () {
    lazyImg();
    innerScroll();
    sliders();
});

$(window).on('load', function () {
  animations();
  lazyImgLoad();
  bgimg();
});

$(window).on('resize', function () {
});

$(document).on('scroll', function () {

});

function lazyImg(){
  if($('.lazy').length){
    $('.lazy').lazy();
  }
}
function lazyImgLoad(){
  if($('.lazy-load').length){
    $('.lazy-load').Lazy({
      effect: "fadeIn",
      effectTime: 1000,
      threshold: 0,
      delay: 0
    });
  }
}
function bgimg() {
  if($('.bg-load').length){
    $('.bg-load').each(function() {
      var attr = $(this).attr('data-src');
      if (typeof attr !== typeof undefined && attr !== false) {
        $(this).css('background-image', 'url('+attr+')');
      }
    });
  }
}

// Inner Scroll
function innerScroll(){
  $('a.inner-scroll[href*="#"]:not([href="#"])').on('click', function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            if($(this).hasClass('has-sticky')){
                $('html, body').animate({
                  scrollTop: target.offset().top-$('.article-nav-sticky').innerHeight()-$('.main-header').innerHeight()
              }, 1000);
              return false;
            }else{
                $('html, body').animate({
                    scrollTop: target.offset().top-$('.main-header').innerHeight()
                }, 1000);
                return false;
            }
          }
      }
  });
}

// Fancybox
function fancy(){
  if($('[data-fancybox]').length){
    $('[data-fancybox]').fancybox({
      baseTpl:
        '<div class="fancybox-container" role="dialog" tabindex="-1">' +
        '<div class="fancybox-bg"></div>' +
        '<div class="fancybox-toolbar">{{buttons}}</div>' +
        '<div class="fancybox-inner z-1">' +
        '<div class="fancybox-infobar white all d-inline-flex align-items-center">' +
        '<span class="fancy-title bold lh-1-2 h3 pr-xxl-100 pr-xl-70 pr-md-40 pr-15"></span>' +
        '<span data-fancybox-index class="item-length medium lh-1-1 h5 small d-inline-block"></span>' +
        '<span class="item-length fancy-line opacity-4 ml-5 mr-5 d-inline-block h6">|</span>' +
        '<span data-fancybox-count class="item-length medium lh-1-1 h5 small opacity-4 d-inline-block"></span>'+
        '</div>' +
        '<div class="fancybox-navigation">{{arrows}}</div>' +
        '<div class="fancybox-stage"></div>' +
        '<div class="fancybox-caption"><div class="fancybox-caption__body p xs italic lh-1-8 white opacity-6"></div></div>' +
        "</div>" +
        "</div>",
      buttons: ["close", "thumbs"],
      toolbar: "true",
      thumbs : {
        autoStart : true
      },
      beforeLoad : function( instance, slide ) {
        // Hide Elements
        if(instance.group.length<=1){
          $('.item-length, .fancybox-navigation').removeClass('d-inline-block').addClass('d-none');
        }else{
          $('.item-length, .fancybox-navigation').removeClass('d-none').addClass('d-inline-block');
        }
        // Add Title
        try{
          var title = slide.opts.$orig[0].getAttribute('data-title');
          if(title.length){
            $('.fancy-title').removeClass('d-none').addClass('d-inline-block').text(title);
          }else{
            $('.fancy-title').removeClass('d-inline-block').addClass('d-none');
          }
        }catch(e){

        }
      },
      btnTpl: {
        // Arrows
        arrowLeft:
          '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' +
          '<div class="icon-arrow-left white icon-radius big"></div>' +
          "</button>",
    
        arrowRight:
          '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' +
          '<div class="icon-arrow-right white icon-radius big"></div>' +
          "</button>"
      }
    });
  }
}

// Sliders
function sliders(){
    if($('.each-full-slider').length){
        var $carouselFull = $('.slider-full-texts');
        var $carouselFullNav = $('.slider-full-images');

        $carouselFull.slick({
            arrows: true,
            dots: true,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            adaptiveHeight: true,
            appendDots: $carouselFull.closest('.each-full-slider').find(".slider-dots"),
            appendArrows: $carouselFull.closest('.each-full-slider').find(".slider-arrows"),
            prevArrow: "<a href='javascript:void(0)' class='d-inline-block icon-arrow-left black' role='button'></a>",
            nextArrow: "<a href='javascript:void(0)' class='d-inline-block icon-arrow-right black' role='button'></a>",
            asNavFor: $carouselFullNav
        });

        $carouselFullNav.slick({
            dots: false,
            arrows: false,
            infinite: false,
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: $carouselFull
        });
    }



    if($('.slider-1-fade').length){
      var $fadeSlider = $('.slider-1-fade');
      var $slideSlider = $('.slider-1-slide');

      $slideSlider.each(function(){
        $(this).slick({
            arrows: true,
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: false,
            adaptiveHeight: true,
            asNavFor: $(this).closest('.each-full-slider').find('.slider-1-fade'),
            appendArrows: $(this).closest('.each-full-slider').find(".slider-arrows"),
            prevArrow: "<a href='javascript:void(0)' class='d-inline-block icon-arrow-left white icon-radius mr-5 small' role='button'></a>",
            nextArrow: "<a href='javascript:void(0)' class='d-inline-block icon-arrow-right white icon-radius small' role='button'></a>"
        });
      });
      $fadeSlider.each(function(){
        $(this).slick({
            dots: false,
            arrows: false,
            infinite: false,
            fade: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: $(this).closest('.each-full-slider').find('.slider-1-slide')
          });
        });
    }
    if($('.slider-slide-3').length){
      var $slideSlider = $('.slider-slide-3');
      var $slideSliderNav = $('.slider-slide-3-nav');

      $slideSlider.each(function(){
        $(this).slick({
            arrows: true,
            dots: false,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: false,
            centerMode: false,
            speed: 300,
            cssEase: 'linear',
            asNavFor: $(this).closest('.each-full-slider').find($slideSliderNav),
            appendArrows: $(this).closest('.each-full-slider').find(".slider-arrows"),
            prevArrow: "<a href='javascript:void(0)' class='d-inline-block icon-arrow-left greyish-brown-three french-blue-hover icon-opacity icon-radius big mr-5' role='button'></a>",
            nextArrow: "<a href='javascript:void(0)' class='d-inline-block icon-arrow-right greyish-brown-three french-blue-hover icon-opacity icon-radius big' role='button'></a>"
        });
      });

      $slideSliderNav.each(function(){
        $(this).slick({
            dots: false,
            arrows: false,
            infinite: true,
            fade: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            swipe:false,
            draggable: false,
            speed: 300,
            cssEase: 'linear',
            asNavFor: $(this).closest('.each-full-slider').find($slideSlider)
        });

        $slideSliderNav.find('.slick-slide').on('click',function(e){
          e.preventDefault();
          slideIndex = $(this).data('slick-index');
          $slideSliderNav.slick('slickGoTo', slideIndex);
        });
      });
    }

    if($('.full-width-carousel').length){
      var $carousel = $('.full-width-carousel');
      var $carouselNav = $('.full-width-carousel-nav');

      $carousel.each(function(){
        $(this).slick({
            variableWidth: true,
            arrows: true,
            dots: false,
            infinite: true,
            asNavFor: $(this).closest('.each-full-slider').find($carouselNav),
            appendArrows: $(this).closest('.each-full-slider').find(".slider-arrows"),
            prevArrow: "<a href='javascript:void(0)' class='d-inline-block icon-arrow-left greyish-brown-three french-blue-hover icon-opacity icon-radius big mr-5' role='button'></a>",
            nextArrow: "<a href='javascript:void(0)' class='d-inline-block icon-arrow-right greyish-brown-three french-blue-hover icon-opacity icon-radius big' role='button'></a>"
        });
      });
      $carouselNav.each(function(){
        $(this).slick({
            dots: false,
            arrows: false,
            infinite: true,
            fade: true,
            slidesToShow: 1,
            asNavFor: $(this).closest('.each-full-slider').find($carousel)
        });
      });

    }
 
}

function animations(){
    // Init ScrollMagic
    var scrollMagicController = new ScrollMagic.Controller();

    // vars
    var eachContainer = $('.each-section')

    // Parallax 
    if(eachContainer.length){
        var section = eachContainer;
        for (var i=0; i<section.length; i++) {
            // Vars
            var parallaxAnim = $(section[i]).find('.each-parallax');
            // var imageAnim = 

            if(parallaxAnim.length){
              var parallax = new TimelineMax();
              parallax.to(parallaxAnim, 0.6, {y:$(section[i]).height() - parallaxAnim.innerHeight(),ease:Power3.easeIn});
              new ScrollMagic.Scene({
                  triggerElement: section[i],
                  triggerHook: 1,
                  duration: $(section[i]).height()
              })
              .addTo(scrollMagicController)
              // .addIndicators()
              .setTween(parallax);
            }
        }
    }

    // Progress Bar
    if($('.article-line').length){
      // Total width convert to percentage
      var totWidth = (($('.article-nav-sticky').innerWidth()-$('.article-nav-sticky .icon-pin').innerWidth()) / $(window).width())*100;

      new ScrollMagic.Scene({
          triggerElement: '#article-line',
          triggerHook: 0.5,
          duration: $('.article-line').height()
      })
      .addTo(scrollMagicController)
      // .addIndicators()
      .setTween($('.progressbar'), 0.6, {width:totWidth+"%"});

      new ScrollMagic.Scene({
        triggerElement: '#article-line',
        triggerHook: 0.5,
      })
      .setClassToggle('.article-nav-sticky', 'active')
      // .addIndicators()
      .addTo(scrollMagicController);
    }
}