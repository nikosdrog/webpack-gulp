import popper from "popper.js";
import jquery from "jquery";
import bootstrap from "bootstrap";
import lazy from "jquery-lazy";
import mousewheel from "jquery-mousewheel";
import customScrollbar from "malihu-custom-scrollbar-plugin";
import slickCarousel from "slick-carousel";
import fancybox from "@fancyapps/fancybox";
import { TimelineMax, TweenMax, Linear } from 'gsap';
import ScrollMagic from 'scrollmagic';
import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap';
// Remove debug.addIndicators when project go live
import 'imports-loader?define=>false!scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators';

require('./remove-accents.min.js');
require('./jquery.ellipsis.js');


