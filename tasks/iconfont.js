var gulp = require("gulp");
var iconfont = require('gulp-imagemin');
var iconfont = require("gulp-iconfont");
var iconfontCSS = require('gulp-iconfont-css');

var fontName = 'icons';

gulp.task('iconfont', function () {
  gulp.src(['./dev/fonts/icons/*.svg'])
    .pipe(iconfontCSS({
      fontName: fontName,
      path: './dev/sass/_iconfont_template.scss',
      targetPath: '../sass/parts/vendor/_icons.scss',
      fontPath: '../fonts/'
    }))
    .pipe(iconfont({
      prependUnicode: true,
      fontName: fontName,
      // Remove woff2 if you get an ext error on compile
      formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
      normalize: true,
      fontHeight: 1001
    }))
    .pipe(gulp.dest('./dev/fonts/'));
});

// gulp.task('icons', ['iconfont', 'scss']);
// gulp.task('scss', function(){
//   gulp.src('scss/**/*.scss')
//     .pipe(sass())
//     .pipe(autoprefixer()) // Requires the gulp-autoprefixer plugin
//     .pipe(gulp.dest('css/'))
//     .pipe(notify({ message: 'SCSS task complete' })) // Requires gulp-notify
// });