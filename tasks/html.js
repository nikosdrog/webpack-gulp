/**
 * Build HTML
 * Build Custom JS
 */
const gulp = require('gulp');
const inject = require('gulp-inject');
const { reload } = require('browser-sync');
const fileinclude = require('gulp-file-include');
const notify = require('gulp-notify');
const path = require('path');
const plumber = require('gulp-plumber');

const config = require('./config');


// html paths
var paths = {
  index : './dist/*.html',
  allJS: './dev/static/js/*.js',
  allCSS: './dev/static/css/*.css'
};

// Markup Task
gulp.task('markup', () =>
  gulp
    .src(path.join(config.root.dev, config.html.dev, './*.html'))
    .pipe(plumber({ errorHandler: notify.onError('Error: <%= error.message %>') }))
    .pipe(fileinclude({
      prefix: '@@',
      basepath: path.join(
        config.root.dev,
        config.html.dev,
        config.html.parts,
      ),
    }))
    .pipe(gulp.dest(path.join(config.root.dist, config.html.dist)))
    .pipe(reload({ stream: true })));


// Add Libraries (JS, CSS) one after other
gulp.task('files', ['markup'] , () => {
  gulp
      .src(paths.index) // 1 - Getting the index path as source !
      .pipe(inject(gulp.src([paths.allJS,paths.allCSS], {read: false}), {ignorePath: 'dev'})) // 2 - indicating that we need to inject all file with .js and .css extension.
      .pipe(gulp.dest(path.join(config.root.dist, config.html.dist))); // 3 - Simply exporting everything to a file that holds the same name in a diffrent folder.
});


// Complete tasks
gulp.task('html', ['files']);