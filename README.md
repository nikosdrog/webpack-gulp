# Contents
 1. [Libraries](#libraries)
 2. [Installation](#installation)
 3. [Boilerplate](#boilerplate)
	 - [Commands](#commands)
	 - [Starting Development](#starting-development)
4. [Folder Structure](#folder-structure)
5. [Rest CSS](#rest-css)
6. [Deployment](#deployment)
 

## Libraries

 - jQuery 3 https://jquery.com/
 - Bootstrap 4 https://getbootstrap.com/
 - Lazy Load http://jquery.eisbehr.de/lazy/
 - Custom Scrollbar http://manos.malihu.gr/jquery-custom-content-scroller/
 - GSAP https://greensock.com/gsap
 - Slick http://kenwheeler.github.io/slick/
 - Fancybox 3 https://fancyapps.com/fancybox/3/
 - Scrollmagic http://scrollmagic.io/

## Installation

Install Visual Studio Code https://code.visualstudio.com/ <br>
Install Node.js (Recommended to use node >=8.9) <br>
Restart your computer <br>

After restart open Visual Studio Code and open terminal (ctrl + ~) <br>
Go to the folder you want to put the boilerplate by typing with the cd command
```bash
git clone https://gitlab.com/nikosdrog/webpack-gulp my-project-name / and download your repo.
cd my-project-name
npm install
```

If got gyp ERR! stack Error: Can't find Python executable "python", you can set the PYTHON env variable. Follow the steps below
```text
If you haven't got python installed along with all the node-gyp dependencies, simply open Powershell or Git Bash with administrator privileges and execute:

npm install --global --production windows-build-tools
and then to install the package:

npm install --global node-gyp
once installed, you will have all the node-gyp dependencies downloaded, but you still need the environment variable. Validate Python is indeed found in the correct folder:

C:\Users\ben\.windows-build-tools\python27\python.exe 
Note - it uses python 2.7 not 3.x as it is not supported

If it doesn't moan, go ahead and create your (user) environment variable:

setx PYTHON "%USERPROFILE%\.windows-build-tools\python27\python.exe"
restart cmd, and verify the variable exists via set PYTHON which should return the variable

After that run npm Install again
```


## Boilerplate

### Commands
```bash
npm start //Run development mode
npm run-script build //Compiles your App for production
``` 
<br>

### Starting Development
```bash
npm start
```
After npm start for first time only go to (BUG) go to dev/js/modules and open anyone file and just save it.
Same as JS you must do it for CSS, go to dev/sass/ and open anyone file and just save it.
<br>


#### Folder Structure
``` text
├── dev
│   ├── fonts
│   │   ├── icons
│   ├── html
│   │   ├── nodes
│   ├── img
│   ├── js
│   │   ├── modules
│   ├── sass
│   │   ├── parts
│   ├── static
│   │   ├── css
│   │   ├── js
```

**fonts->icons**
Add all svg's icons. To generate the font write npm run-script build<br>

**html->nodes**
Add all elements as plain html<br>

**js->modules**
For npm libraries
1. Write to command eq. `npm i bootstrap`
2. go to index.js and add
```js
import bootstrap from "bootstrap";
```

No npm libraries
1. Add JS file inside modules
2. go to index.js and add
```js
require('./file-name.js');
```

**sass->parts**
Everything you want for SASS helpers, style-guide libraries

**static->js - static-css**
If you want to code something please add CSS or JS file. All you have to do to show it to save any HTML file only the first time since the creation of.

## Rest CSS

### Margins Paddings

```css
.mt-(sm,md,lg,xl,xxl)-(0-200) {margin-top}
.mr-(sm,md,lg,xl,xxl)-(0-200) {margin-right}
.mb-(sm,md,lg,xl,xxl)-(0-200) {margin-bottom}
.ml-(sm,md,lg,xl,xxl)-(0-200) {margin-left}

.pt-(sm,md,lg,xl,xxl)-(0-200) {padding-top}
.pr-(sm,md,lg,xl,xxl)-(0-200) {padding-right}
.pb-(sm,md,lg,xl,xxl)-(0-200) {padding-bottom}
.pl-(sm,md,lg,xl,xxl)-(0-200) {padding-left}
```


### Aspect Ratio

These classes add a percentage padding-top at the element. This way the height is calculated based on the width of the element keeping its aspect ratio in any screen.
```css
.ar-(sm,md,lg,xl,xxl)-(0-100) {padding-top: 0-100%}
```
 
### Displays and Positions

Classes for easy positioning and display elements:
```css
.(sm,md,lg,xl,xxl)-relative{position:relative;}
.(sm,md,lg,xl,xxl)-absolute{position:absolute;}
.(sm,md,lg,xl,xxl)-static{position:static;}
```

### Bootstrap Extras

Extra bootstrap container:<br>
Container on xxl is 1600px width. The brakepoint is on 1660px
